Sub OpenFile(sNamePath) ''/*{{{*/
    ' OpenFile
    '   Opens file
    '
    '  ARGS:
    '   STRING  :       sNamePath       = path + filename
    '
    ' By:       John Kaul
    '           09.15.15
    Jrn.Command "Internal" , "Open an existing project , ID_REVIT_FILE_OPEN"
    Jrn.Data "File Name" _
        , "IDOK", sNamePath
End Sub ''/*}}}*/
