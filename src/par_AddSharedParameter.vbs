Sub AddSharedParameter(sParameter, sSharedParameterGroup, sFamilyGroup, sInstance) ''/*{{{*/
    ' AddSharedParameter
    '
    ' ARGS:
    '   STRING  : sParameter                    =       Parameter Name to add
    '   STRING  : sSharedParameterGroup         =       Group in Shared parameter file
    '                                                   sParameter can be found
    '   STRING  : sFamilyGoup                   =       A string representing the parameter
    '                                                   group should be placed in.
    '   STRING  : sInstance                     =       A string representing the type;
    '                                                   "Instance"  or  "Type"
    '
    ' EX: AddParameter "Design Supply Airflow", "Mechanical - Airflow", "Mechanical", "Instance"
    '
    ' By:       John Kaul
    '           09.15.15
        Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
                 , "Add..., Control_Family_NewFamParam"
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "Shared parameter, Control_Revit_ExternalParam"
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "Select..., Control_Revit_ExternalParamSelect"
        Jrn.ComboBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                 , "Control_Revit_ParameterGroup" _
                 , "SelEndOk" , sSharedParameterGroup
        Jrn.ComboBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                 , "Control_Revit_ParameterGroup" _
                 , "Select" , sSharedParameterGroup
        Jrn.ListBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                 , "Control_Revit_Parameters" _
                 , "Select" , sParameter
        Jrn.PushButton "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                 , "OK, IDOK"
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "Control_Revit_ParamGroup" _
                 , "Select" , sFamilyGroup
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , sInstance & ", Control_Revit_Radio" & sInstance
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "OK, IDOK"
        Jrn.Data "Transaction Successful"  _
                , "New param"
End Sub ''/*}}}*/
