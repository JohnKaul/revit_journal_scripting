Sub CloseFile() ''/*{{{*/
    ' CloseFile
    '   Closes a file
    '
    ' By:       John Kaul
    '           09.15.15
    Jrn.Command "Menu" , "Close the active project , ID_REVIT_FILE_CLOSE"
End Sub ''/*}}}*/
