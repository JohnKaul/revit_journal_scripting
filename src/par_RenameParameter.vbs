Sub RenameParameter(sParameter, sNewName)       ''/*{{{*/
    ' Rename Parameter
    '   Renames a parameter.
    '
    ' ARGS:
    '   STRING  :       sParameter      =       Parameter name to rename
    '   STRING  :       sNewName        =       New name for Parameter
    '
    ' EX: Rename Parameter "Coil Water Return Radius", "Chilled Water Return Radius"
    '
    ' By:       John Kaul
    '           09.23.15

        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "Selection" , ""
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sParameter , "NameCol"
        Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
                , "Modify..., Control_Family_EditFamParam"
        Jrn.Edit "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Control_Revit_Name" _
                , "ReplaceContents" , sNewName
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "OK, IDOK"
        Jrn.Data "Transaction Successful"  _
                , "Edit param"
End Sub ''/*}}}*/
