Sub AddFamilyParameter(sParameter, sParameterDiscipline, sParameterType, sFamilyGroup, sInstance) ''/*{{{*/
    ' AddFamilyParameter
    '
    ' ARGS:
    '   STRING  : sParameter                    =       Parameter Name to add
    '   STRING  : sFamilyGoup                   =       A string representing the parameter
    '                                                   group should be placed in.
    '   STRING  : sParameterType                =       Type of parameter.
    '   STRING  : sInstance                     =       A string representing the type;
    '                                                   "Instance"  or  "Type"
    '
    ' EX: AddFamilyParameter "Distance", "Common", "Length", "Instance"
    '
    ' By:       John Kaul
    '           09.15.15
        Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
                , "Add..., Control_Family_NewFamParam"
        Jrn.Edit "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Control_Revit_Name" _
                , "ReplaceContents" , sParameter
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Control_Revit_ParamDiscipline" _
                , "Select" , sParameterDiscipline
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Control_Revit_ParamType" _
                , "SelEndOk" , sParameterType 
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Control_Revit_ParamType" _
                , "Select" , sParameterType
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "Control_Revit_ParamGroup" _
                 , "Select" , sFamilyGroup
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , sInstance & ", Control_Revit_Radio" & sInstance
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "OK, IDOK"
        Jrn.Data "Transaction Successful"  _
                , "New param"
End Sub ''/*}}}*/
