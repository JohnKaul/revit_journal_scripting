Sub MapToSharedParameter(sExistParameter, sMappedParameter, sSharedParameterGroup, sFamilyGroup, sInstance) ''/*{{{*/
    ' MapToSharedParameter
    '
    ' ARGS:
    '   STRING  : sExistParameter               =       Parameter Name to modify
    '   STRING  : sMappedParameter              =       Parameter to map to
    '   STRING  : sSharedParameterGroup         =       Group in Shared parameter file
    '                                                   sParameter can be found
    '   STRING  : sFamilyGoup                   =       A string representing the parameter
    '                                                   group should be placed in.
    '   STRING  : sInstance                     =       A string representing the type;
    '                                                   "Instance"  or  "Type"
    ' 
    ' REQD:
    '  OpenFamilyTypesDialog    :       If the dialog wasn't already open this function will not work.
    '   EG:
    '           OpenFamilyTypesDialog()
    '                   MapToFamilyParameter ...
    '
    ' EX: ModifyParameter "Voltage", "Voltage", "Electrical", "Electrical", "Type"
    '
    ' By:       John Kaul
    '           09.15.15
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sExistParameter , "NameCol"
        Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
                , "Modify..., Control_Family_EditFamParam"
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Shared parameter, Control_Revit_ExternalParam"
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Select..., Control_Revit_ExternalParamSelect"
        Jrn.ComboBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "Control_Revit_ParameterGroup" _
                , "SelEndOk" , sSharedParameterGroup
        Jrn.ComboBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "Control_Revit_ParameterGroup" _
                , "Select" , sSharedParameterGroup
        Jrn.ListBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "Control_Revit_Parameters" _
                , "Select" , sMappedParameter
        Jrn.PushButton "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "OK, IDOK"
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "Control_Revit_ParamGroup" _
                 , "Select" , sFamilyGroup
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , sInstance & ", Control_Revit_Radio" & sInstance
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "OK, IDOK"
        Jrn.Data "Transaction Successful"  _
                , "New param"
End Sub ''/*}}}*/
Sub MapToSharedParameter(sExistParameter, sSharedParameterGroup, sFamilyGroup, sInstance) ''/*{{{*/
    ' MapToSharedParameter
    '
    ' ARGS:
    '   STRING  : sExistParameter               =       Parameter Name to modify
    '   STRING  : sMappedParameter              =       Parameter to map to
    '   STRING  : sSharedParameterGroup         =       Group in Shared parameter file
    '                                                   sParameter can be found
    '   STRING  : sFamilyGoup                   =       A string representing the parameter
    '                                                   group should be placed in.
    '   STRING  : sInstance                     =       A string representing the type;
    '                                                   "Instance"  or  "Type"
    ' 
    ' REQD:
    '  OpenFamilyTypesDialog    :       If the dialog wasn't already open this function will not work.
    '   EG:
    '           OpenFamilyTypesDialog()
    '                   MapToFamilyParameter ...
    '
    ' EX: ModifyParameter "Voltage", "Voltage", "Electrical", "Electrical", "Type"
    '
    ' By:       John Kaul
    '           09.15.15
        if (sInstance = "Instance") then
            sMappedParameter = sExistParameter & " (default)"
        Else 
            sMappedParameter = sExistParameter
        end if

        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sMappedParameter , "NameCol"
        Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
                , "Modify..., Control_Family_EditFamParam"
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Shared parameter, Control_Revit_ExternalParam"
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Select..., Control_Revit_ExternalParamSelect"
        Jrn.ComboBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "Control_Revit_ParameterGroup" _
                , "SelEndOk" , sSharedParameterGroup
        Jrn.ComboBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "Control_Revit_ParameterGroup" _
                , "Select" , sSharedParameterGroup
        Jrn.ListBox "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "Control_Revit_Parameters" _
                , "Select" , sExistParameter
        Jrn.PushButton "Modal , Shared Parameters , Dialog_Revit_ExternalParamImport" _
                , "OK, IDOK"
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "Control_Revit_ParamGroup" _
                 , "Select" , sFamilyGroup
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , sInstance & ", Control_Revit_Radio" & sInstance
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "OK, IDOK"
        Jrn.Data "Transaction Successful"  _
                , "New param"
End Sub ''/*}}}*/
