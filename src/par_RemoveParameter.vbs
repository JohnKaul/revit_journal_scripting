Sub RemoveParameter(sParameter) ''/*{{{*/
    ' RemoveParameter
    '   Removes a parameter.
    '
    ' ARGS:
    '   STRING  :       sParameter      =       Paramter name to remove
    '
    ' EX: RemoveParameter "Design Supply Airflow (default)"
    '
    ' By:       John Kaul
    '           09.15.15
    Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
        , "MoveCurrentCell" , sParameter , "ValueCol"
    Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
        , "Remove, Control_Family_DeleteFamParam"
    Jrn.Data "TaskDialogResult"  _
        , "Delete family parameter?",  _
        "Yes", "IDYES"
    Jrn.Data "Transaction Successful"  _
        , "Delete param"
End Sub ''/*}}}*/
Sub RemoveParameter(sParameter, bInstance) ''/*{{{*/
    ' RemoveParameter
    '   Removes a parameter.
    '
    ' ARGS:
    '   STRING  :       sParameter      =       Paramter name to remove
    '   BOOLE   :       bInstance       =       True False if parameter is instance based
    '
    ' EX: RemoveParameter "Design Supply Airflow", True
    '
    ' By:       John Kaul
    '           09.15.15
    if (bInstance) then
        sParameter = sParameter & " (default)"
    end if
    Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
        , "MoveCurrentCell" , sParameter , "ValueCol"
    Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
        , "Remove, Control_Family_DeleteFamParam"
    Jrn.Data "TaskDialogResult"  _
        , "Delete family parameter?",  _
        "Yes", "IDYES"
    Jrn.Data "Transaction Successful"  _
        , "Delete param"
End Sub ''/*}}}*/
