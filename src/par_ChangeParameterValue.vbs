Sub ChangeParameterValue (sParameter, sValue)    ''/*{{{*/
    ' Change Parameter Value
    '
    ' ARGS:
    '   STRING  :       sParameter      =       Parameter name to modify
    '   STRING  :       sValue          =       Value to change to
    '
    ' EX:  ChangeParameterValue "Interior Finish", "WHITE"
    '
    ' BY:       John Kaul
    '           09.24.15
    sParameter = CStr(sParameter)
    sValue = CStr(sValue)

        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "Selection" , ""
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sParameter , "ValueCol"
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "PartialEdit" , sParameter , "ValueCol" , sValue, Len(sValue), Len(sValue)
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sParameter , "ValueCol"
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sParameter , "FormulaCol"
End Sub ''/*}}}*/
'' Sub ChangeParameterValue (sParameter, bInstance, sValue)    ''/*{{{*/
''     ' Change Parameter Value
''     '
''     ' ARGS:
''     '   STRING  :       sParameter      =       Parameter name to modify
''     '   BOOLE   :       bInstance       =       TRUE/FALSE if parameter is instance based
''     '   STRING  :       sValue          =       Value to change to
''     '
''     ' EX:  ChangeParameterValue "Interior Finish", True, "WHITE"
''     '
''     ' BY:       John Kaul
''     '           09.24.15
''         if (bInstance) then
''             sParameter = sParameter & " (default)"
''         end if
'' 
''         Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
''                 , "Selection" , ""
''         Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
''                 , "PartialEdit" , sParameter , "ValueCol" , sValue , Len(sValue), Len(sValue)
''         Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
''                 , "MoveCurrentCell" , sParameter , "ValueCol"
''         Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
''                 , "MoveCurrentCell" , sParameter , "FormulaCol"
'' End Sub ''/*}}}*/
