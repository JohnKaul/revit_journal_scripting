Sub MapToFamilyParameter(sExistParameter, sFamilyGroup, sInstance) ''/*{{{*/
    ' MapToFamilyParameter
    '
    ' ARGS:
    '   STRING  : sExistParameter               =       Parameter Name to modfiy
    '   STRING  : sFamilyGoup                   =       A string representing the parameter
    '                                                   group should be placed in.
    '   STRING  : sInstance                     =       A string representing the type;
    '                                                   "Instance"  or  "Type"
    ' REQD:
    '  OpenFamilyTypesDialog    :       If the dialog wasn't already open this function will not work.
    '   EG:
    '           OpenFamilyTypesDialog()
    '                   MapToFamilyParameter ...
    '
    ' EX: MapToFamilyParameter "Voltage", "Electrical", "Type"
    '
    ' By:       John Kaul
    '           09.15.15
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sExistParameter , "NameCol"
        Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
                , "Modify..., Control_Family_EditFamParam"
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Family parameter, Control_Revit_FamilyParam"
        Jrn.ComboBox "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "Control_Revit_ParamGroup" _
                 , "Select" , sFamilyGroup
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , sInstance & ", Control_Revit_Radio" & sInstance
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "OK, IDOK"
        Jrn.Data "Transaction Successful"  _
                , "New param"
End Sub ''/*}}}*/
Sub MapToFamilyParameter(sExistParameter) ''/*{{{*/
    ' MapToFamilyParameter
    '
    ' ARGS:
    '   STRING  : sExistParameter               =       Parameter Name to modfiy
    '
    ' REQD:
    '  OpenFamilyTypesDialog    :       If the dialog wasn't already open this function will not work.
    '   EG:
    '           OpenFamilyTypesDialog()
    '                   MapToFamilyParameter ...
    '
    ' EX: MapToFamilyParameter "Voltage"
    '
    ' By:       John Kaul
    '           09.15.15
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sExistParameter , "NameCol"
        Jrn.PushButton "Modal , Family Types , Dialog_Family_FamilyType" _
                , "Modify..., Control_Family_EditFamParam"
        Jrn.RadioButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                , "Family parameter, Control_Revit_FamilyParam"
        Jrn.PushButton "Modal , Parameter Properties , Dialog_Revit_ParamPropertiesFamily" _
                 , "OK, IDOK"
        Jrn.Data "Transaction Successful"  _
                , "New param"
End Sub ''/*}}}*/
