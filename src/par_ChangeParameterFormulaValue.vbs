Sub ChangeParameterFormulaValue (sParameter, sValue)    ''/*{{{*/
    ' Change Parameter Value
    '
    ' ARGS:
    '   STRING  :       sParameter      =       Parameter name to modify
    '   STRING  :       sValue          =       Value to change to
    '
    ' EX:  ChangeParamterFormulaValue "Interior Finish", "WHITE"
    '
    ' BY:       John Kaul
    '           09.24.15
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "PartialEdit" , sParameter , "FormulaCol" , sValue, Len(sValue), Len(sValue)
        Jrn.Grid "Control; Modal , Family Types , Dialog_Family_FamilyType; Control_Family_TypeGrid" _
                , "MoveCurrentCell" , sParameter , "FormulaCol"
End Sub ''/*}}}*/
