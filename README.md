# README #

### How do I get set up? ###

* Clone this project or download an archive of this project. Check the downloads section of this project for releases.
*Downloads*: https://bitbucket.org/JohnKaul/revit_journal_scripting/downloads

### Contribution guidelines ###

* Contribute? Please. Feel free.
* Code review? Yes, please.
* Comments? Yes, please.
* Testing
* Code review
* Guidelines
* Currently, the releases are built using `make` and a tool called `LiFP`.

### Git Standards ###
Each commit will be structured like this:
`"file name: Formatting cleanup."`
Where the file name is listed first followed by a colon and a brief description of the change.

### Coding Standards ###
1. Each time the file is updated a line in the file (`## File Last Updated: ` should also be updated. I use a simple vim mapping for this.

### Who do I talk to? ###

* John Kaul - john.kaul@outlook.com